﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Low.Models;

namespace Task24_Low.Content
{
    public static class CustomHelper
    {
        public static MvcHtmlString ConvertToList(this HtmlHelper helper, Form form)
        {
            var div = new TagBuilder("div");
            string result = String.Empty;
            result += "<ul class=\"result-text\">";
            result += "<li>Name: " + form.Name + "</li>";
            result += "<li>Surname: " + form.Surname + "</li>";
            result += "<li>Email: " + form.Email + "</li>";
            result += "<li>Gender: " + form.Gender + "</li>";
            result += "<li>Agreement: " + form.Agreement + "</li>";
            div.InnerHtml = result;
            var html = div.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(html);
        }
    }
}