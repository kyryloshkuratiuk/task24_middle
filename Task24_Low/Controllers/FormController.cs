﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Task24_Low.Models;

namespace Task24_Low.Controllers
{
    public class FormController : Controller
    {
        readonly IRepository<Form> db;

        static List<string> validationErrors = new List<string>();

        /// <summary>
        /// FormController constructor
        /// </summary>
        public FormController()
        {
            db = new SQLFormRepository();
        }

        /// <summary>
        /// Action to redirect to main page
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Action to redirect to result page
        /// </summary>
        [HttpGet]
        public ActionResult Result()
        {
            return View(db.GetList());
        }

        /// <summary>
        /// Action to add a new form to the database
        /// </summary>
        [HttpPost]
        public ActionResult Create()
        {
            validationErrors = new List<string>();

            bool flag = true;
            Regex nameRegex = new Regex("[A-Za-z]+");

            try
            {
                Match match = nameRegex.Match(Request.Form["FirstName"]);
                if (!match.Success)
                {
                    validationErrors.Add("Provided name is not valid.");
                    flag = false;
                }

                match = nameRegex.Match(Request.Form["LastName"]);
                if (!match.Success)
                {
                    validationErrors.Add("Provided surname is not valid");
                    flag = false;
                }

                Regex emailRegex = new Regex(@"(\w.?)+@[A-Za-z]+.[a-z]+");
                match = emailRegex.Match(Request.Form["Email"]);
                if (!match.Success)
                {
                    validationErrors.Add("Provided email is not valid");
                    flag = false;
                }

                if (Request.Form["Gender"] == null)
                {
                    validationErrors.Add("Provided gender is not valid");
                    flag = false;
                }
            }
            catch (Exception e)
            {
                flag = false;
                validationErrors.Add(e.Message);
            }

            string agreement = String.Empty;
            if (Request.Form["Agreement"] == "false")
                agreement = "Rejected";
            else agreement = "Accepted";

            if (flag == true && ModelState.IsValid)
            {
                db.Create(
                new Form
                {
                    Name = Request.Form["FirstName"],
                    Surname = Request.Form["LastName"],
                    Email = Request.Form["Email"],
                    Gender = Request.Form["Gender"],
                    Agreement = agreement
                });

                db.Save();

                return RedirectToAction("Result");
            }
            else return RedirectToAction("Error");
        }

        /// <summary>
        /// Action to redirect to Error page
        /// </summary>
        public ActionResult Error()
        {
            return View(validationErrors);
        }
    }
}