﻿using System;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Task24_Low.Models;
using Task24_Low.Repositories;

namespace Task24_Low.Controllers
{
    public class GuestController : Controller
    {
        readonly IRepository<Review> db;

        /// <summary>
        /// GuestController contstructor
        /// </summary>
        public GuestController()
        {
            db = new SQLReviewRepository();
        }

        /// <summary>
        /// Action to redirect to the main page
        /// </summary>
        public ActionResult Index()
        {
            return View(db.GetList());
        }

        /// <summary>
        /// Action to add a new review to the database
        /// </summary>
        [HttpPost]
        public ActionResult Create()
        {
            bool flag = true;
            Regex textRegex = new Regex("[A-Za-z]+");

            try
            {
                Match match = textRegex.Match(Request.Form["name"]);

                if (!match.Success)
                {
                    flag = false;
                }

                match = textRegex.Match(Request.Form["textReview"]);

                if (!match.Success)
                {
                    flag = false;
                }
            }
            catch
            {
                flag = false;
            }

            if (flag == true && ModelState.IsValid)
            {
                db.Create(
                    new Review
                    {
                        AuthorName = Request.Form["name"],
                        Date = DateTime.Now,
                        Content = Request.Form["textReview"]
                    });
                db.Save();
            }

            return RedirectToAction("Index");

        }
    }
}