﻿using System.Web.Mvc;
using Task24_Low.Models;
using Task24_Low.Repositories;


namespace Task24_Low.Controllers
{
    public class HomeController : Controller
    {
        readonly IRepository<Article> db;

        /// <summary>
        /// HomeController constructor
        /// </summary>
        public HomeController()
        {
            db = new SQLArticleRepository();
        }

        /// <summary>
        /// Action to redirect to the main page
        /// </summary>
        public ActionResult Index()
        {
            return View(db.GetList());
        }

        /// <summary>
        /// Action to redirect to the main page
        /// </summary>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Action to add new article to the database
        /// </summary>
        /// <param name="article">Article object to add</param>
        [HttpPost]
        public ActionResult Create(Article article)
        {
            if (ModelState.IsValid)
            {
                db.Create(article);
                db.Save();
                return RedirectToAction("Index");
            }

            return View(article);
        }

        /// <summary>
        /// Action to edit provided article
        /// </summary>
        /// <param name="article">Article to edit</param>
        [HttpPost]
        public ActionResult Edit(Article article)
        {
            if (ModelState.IsValid)
            {
                db.Update(article);
                db.Save();
                return RedirectToAction("Index");
            }

            return View(article);
        }

        /// <summary>
        /// Action to delete an article by ID
        /// </summary>
        /// <param name="id">Article ID</param>
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Article a = db.GetObject(id);
            return View(a);
        }

        /// <summary>
        /// Action to confirm deletion
        /// </summary>
        /// <param name="id">Article ID</param>
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            db.Delete(id);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        /// <param name="disposing">Disposal status</param>
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}