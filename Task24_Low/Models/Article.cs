﻿using System;

namespace Task24_Low.Models
{
    /// <summary>
    /// Entity of article class
    /// </summary>
    public class Article
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}