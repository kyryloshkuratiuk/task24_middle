﻿using System;

namespace Task24_Low.Models
{
    /// <summary>
    /// Entity of Review class
    /// </summary>
    public class Review
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}