﻿using System;
using System.Collections.Generic;

namespace Task24_Low
{
    /// <summary>
    /// Generic repository interface
    /// </summary>
    /// <typeparam name="T">Class parameter</typeparam>
    interface IRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> GetList(); // Get list of all objects
        T GetObject(int id); // Get specific object by ID
        void Create(T item); // Create an object
        void Update(T item); // Update the object
        void Delete(int id); // Delete the object
        void Save(); // Save changes
    }
}
