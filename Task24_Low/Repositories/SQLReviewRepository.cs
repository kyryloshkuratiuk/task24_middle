﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Task24_Low.Models;

namespace Task24_Low.Repositories
{
    public class SQLReviewRepository : IRepository<Review>
    {
        private readonly DataContext db;

        /// <summary>
        /// SQLReviewRepository constructor
        /// </summary>
        public SQLReviewRepository()
        {
            this.db = new DataContext();
        }

        /// <summary>
        /// Method to get IEnumberable of Reviews
        /// </summary>
        public IEnumerable<Review> GetList()
        {
            return db.Reviews;
        }

        /// <summary>
        /// Method to return an object of Review by ID
        /// </summary>
        /// <param name="id">An ID of Review to return</param>
        public Review GetObject(int id)
        {
            return db.Reviews.Find(id);
        }

        /// <summary>
        /// Method to add Review object to the database
        /// </summary>
        /// <param name="item">Item object to add to the database</param>
        public void Create(Review item)
        {
            db.Reviews.Add(item);
        }

        /// <summary>
        /// Method to update the review
        /// </summary>
        /// <param name="item">Object of review to update</param>
        public void Update(Review item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        /// <summary>
        /// Method to delete the review
        /// </summary>
        /// <param name="id">ID of review to delete</param>
        public void Delete(int id)
        {
            Review Review = db.Reviews.Find(id);
            if (Review != null)
                db.Reviews.Remove(Review);
        }

        /// <summary>
        /// Method to save changes in the database
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }

        public bool disposed = false;

        /// <summary>
        /// Disposal method
        /// </summary>
        /// <param name="disposing">Dispoosal boolean</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Disposal method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}