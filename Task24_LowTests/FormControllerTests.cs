﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task24_Low.Controllers;
using System.Web.Mvc;

namespace Task24_LowTests
{
    [TestClass]
    public class FormControllerTests
    {
        [TestMethod]
        public void FormControllerReturnsView_ShouldReturnTrue()
        {
            // Arrange
            FormController controller = new FormController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
